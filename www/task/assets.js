#!/usr/bin/env node

//ejecutar node www/task/assets.js desde la carpeta raiz del proyecto (fuera de www)

var fs = require('fs');
var path = require('path');

var filestocopy = [
  {
    "../resources/android/icon/drawable-hdpi-icon.png":
      "../../platforms/android/res/mipmap-hdpi/icon.png"
  },
  {
    "../resources/android/icon/drawable-ldpi-icon.png":
      "../../platforms/android/res/mipmap-ldpi/icon.png"
  },
  {
    "../resources/android/icon/drawable-mdpi-icon.png":
      "../../platforms/android/res/mipmap-mdpi/icon.png"
  },
  {
    "../resources/android/icon/drawable-xhdpi-icon.png":
      "../../platforms/android/res/mipmap-xhdpi/icon.png"
  },
  {
    "../resources/android/splash/drawable-port-hdpi-screen.png":
      "../../platforms/android/res/drawable-port-hdpi/screen.png"
  },
  {
    "../resources/android/splash/drawable-port-ldpi-screen.png":
      "../../platforms/android/res/drawable-port-ldpi/screen.png"
  },
  {
    "../resources/android/splash/drawable-port-mdpi-screen.png":
      "../../platforms/android/res/drawable-port-mdpi/screen.png"
  },
  {
    "../resources/android/splash/drawable-port-xhdpi-screen.png":
      "../../platforms/android/res/drawable-port-xhdpi/screen.png"
  }
];

// no need to configure below
var rootdir = 'www/task/';

filestocopy.forEach(function(obj) {
  Object.keys(obj).forEach(function(key) {
    var val = obj[key];
    var srcfile = path.join(rootdir, key);
    var destfile = path.join(rootdir, val);
    console.log("copying "+srcfile+" to "+destfile);
    var destdir = path.dirname(destfile);
    console.log(destdir);
    if (fs.existsSync(srcfile) && fs.existsSync(destdir)) {
      fs.createReadStream(srcfile).pipe(
        fs.createWriteStream(destfile));
    }
  });
});
