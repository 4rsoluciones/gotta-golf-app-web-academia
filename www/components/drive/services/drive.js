angular.module('app.drive')

.factory('$driveApi', function ($http, $q, $timeout, $rootScope) {

    //url autorización
    var urlOauth2 = 'https://accounts.google.com/o/oauth2/auth';

    //permisos
    var scopeUrl = 'https://www.googleapis.com/auth/drive';

    //url para solicitar un token
    var urlGetToken = 'https://accounts.google.com/o/oauth2/token';

    //url que genera una solicitud para subir un archivo
    var urlResumable = 'https://www.googleapis.com/upload/drive/v3/files?uploadType=resumable';

    var urlMultipart = 'https://www.googleapis.com/upload/drive/v3/files?uploadType=multipart';

    //datos de configuracion
    var configData = {};

    var getCodeFromUrl = function (url) {
        var regex = /[?&]([^=#]+)=([^&#]*)/g, match;
        while (match = regex.exec(url)) {
            if (match[1] == 'code') return match[2];
        }
        return null;
    };

    var getAccessCode = function () {
        /*var d = $q.defer(), code, browser;

        browser = $cordovaInAppBrowser.open(
            urlOauth2 + '?' +
            'client_id=' + configData.clientId +
            '&response_type=code' +
            '&scope=' + scopeUrl +
            '&redirect_uri=' + configData.redirectUri +
            '&access_type=offline',
            '_blank', {}
        );

        //autorizó la api
        $rootScope.$on('$cordovaInAppBrowser:loadstart', function (e, event) {
            code = getCodeFromUrl(event.url);
            if (code) {
                if (browser) $cordovaInAppBrowser.close();
                d.resolve(code);
            }
        });

        //cierre intencional del navegador
        $rootScope.$on('$cordovaInAppBrowser:exit',function () {
            if (!code) d.reject();
            else d.resolve(code);
        });

        return d.promise;*/
    };

    //obtener token de acceso -> dura una hora y luego hay que renovarlo
    var getAccessToken = function (code) {
        var d = $q.defer();

        $http({
            method: 'POST',
            url: urlGetToken,
            data: 'code=' + code + '&' + 'client_id=' + configData.clientId + '&' + 'client_secret=' + configData.clientSecret + '&' + 'redirect_uri=' + configData.redirectUri + '&grant_type=authorization_code',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(
            function (res) {
                res.data.token_request = new Date().getTime(); //guardo la fecha en milisegundos
                console.log(res.data);
                window.localStorage['DRIVE_DATA'] = JSON.stringify(res.data);
                d.resolve();
            },
            function (err) {
                console.log(err);
                d.reject();
            }
        );

        return d.promise;
    };

    //recuperar access_token al expirar el mismo
    var refreshToken = function () {
        var driveTokenData = JSON.parse(window.localStorage['DRIVE_DATA']);
        var d = $q.defer();
        $http({
            method: 'POST',
            url: urlGetToken,
            data: 'client_id=' + configData.clientId + '&' + 'client_secret=' + configData.clientSecret + '&' + 'refresh_token=' + driveTokenData.refresh_token + '&' + 'grant_type=refresh_token',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(
            function (res) {
                driveTokenData.access_token = res.data.access_token;
                driveTokenData.expires_in = res.data.expires_in;
                driveTokenData.token_type = res.data.token_type;
                driveTokenData.token_request = new Date().getTime(); //guardo la fecha en milisegundos
                window.localStorage['DRIVE_DATA'] = JSON.stringify(driveTokenData);
                d.resolve();
            },
            function (err) {
                console.log(err);
                d.reject();
            }
        );

        return d.promise;
    };

    var verifyToken = function () {
        var driveTokenData = JSON.parse(window.localStorage['DRIVE_DATA']);
        var now = new Date().getTime();
        var diff_dates = now - driveTokenData.token_request;

        return (diff_dates < (driveTokenData.expires_in * 1000));
    };

    var getFileData = function (url, method) {
        var d = $q.defer();

        var directory = '';
        var fileName = url;

        if(method == 'getFromRecorder') {
            fileName = url.split('/')[url.split('/').length - 1];
            directory = cordova.file.dataDirectory;
        }

        $cordovaFile.checkFile(directory, fileName).then(function (res) {
            res.file(function (file) {
                d.resolve(file);
            });
        }, function (err) {
            console.log(err);
            d.reject();
        });
        return d.promise;
    };

    var dataURItoBlob = function (base64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 1024;
        base64Data = base64Data.split(',')[1];
        var byteCharacters = atob(base64Data.replace(/\s/g, ""));
        var bytesLength = byteCharacters.length;
        var slicesCount = Math.ceil(bytesLength / sliceSize);
        var byteArrays = new Array(slicesCount);

        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
            var begin = sliceIndex * sliceSize;
            var end = Math.min(begin + sliceSize, bytesLength);

            var bytes = new Array(end - begin);
            for (var offset = begin, i = 0 ; offset < end; ++i, ++offset) {
                bytes[i] = byteCharacters[offset].charCodeAt(0);
            }
            byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        //return new Blob(byteArrays, { type: contentType });
        return new Blob(byteArrays, {type: "application/octet-stream"});
    }
    function base64MimeType(encoded) {
        var result = null;

        if (typeof encoded !== 'string') {
            return result;
        }

        var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

        if (mime && mime.length) {
            result = mime[1];
        }

        return result;
    }
    function convertDataURIToBinary(dataURI) {
        var BASE64_MARKER = ';base64,';
        var base64Index = dataURI.indexOf(BASE64_MARKER) + BASE64_MARKER.length;
        var base64 = dataURI.substring(base64Index);
        var raw = window.atob(base64);
        var rawLength = raw.length;
        var array = new Uint8Array(new ArrayBuffer(rawLength));

        for(i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array;
    }
    return {

        setConfig: function(config){
            if(config){
                configData = config;
            }
        },

        getCredentials: function () {
            var d = $q.defer();
            if (!window.localStorage.getItem('DRIVE_DATA')) { //primera vez que intenta acceder a la api
                getAccessCode().then(
                    function (code) {
                        getAccessToken(code).then(
                            function () {
                                d.resolve();
                            },
                            function () {
                                d.reject();
                            }
                        );
                    },
                    function () {
                        d.reject();
                    }
                );
            } else {
                if (verifyToken()) {
                    d.resolve();
                }
                else {
                    refreshToken().then(
                        function () {
                            d.resolve();
                        },
                        function () {
                            d.reject();
                        }
                    );
                }
            }

            return d.promise;
        },

        //funcion para subir video
        uploadFile: function (file, method) {

            var driveTokenData = JSON.parse(window.localStorage['DRIVE_DATA']);
            var d = $q.defer();

            data = file.media.path.split(',')[1];
            mimeType = base64MimeType(file.media.path);

            dataForm = "--myFile\nContent-Type: application/json; charset=UTF-8\n\n{\n  \"name\": \""+file.title+"\",\n  \"contentType\": \""+file.media.mimetype+"\",\n  \"parents\": [\""+file.folderId+"\"]\n}\n\n--myFile\nContent-Type: "+file.media.mimetype+"\nContent-Transfer-Encoding: base64\n\n"+data+"\n\n--myFile--\n";

            var options = {
                headers: {
                    'Authorization': 'Bearer ' + driveTokenData.access_token,
                    'Content-Type': 'multipart/related; boundary=myFile'
                }
            };
            $http.post(urlMultipart, dataForm, options).success(
                function(res) {
                    var id = null;
                    try {
                        id = res.id;
                    } catch(e){}
                    d.resolve({id: id, item: file.item});
                }
            ).error(function(err){
                console.log(err);
                d.reject(err);
            });
            

            return d.promise;
        }
    };
});
