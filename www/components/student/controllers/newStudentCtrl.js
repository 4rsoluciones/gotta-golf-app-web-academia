angular.module('app.student')

.controller('NewStudentCtrl', function ($scope, $rootScope, $ionicPlatform, $state, $ionicLoading, $webServices){

    $ionicPlatform.ready(function(){

        $scope.user = [];

        $scope.createStudent = function () {

            if(!$scope.user.firstName){
                alert('Ingrese el nombre');
                //$cordovaToast.show("Ingrese el nombre", 'long', 'bottom');
                return false;
            }
            if (!$scope.user.lastName) {
                alert('Ingrese el apellido');
                //$cordovaToast.show("Ingrese el apellido", 'long', 'bottom');
                return false;
            }
            if (!$scope.user.email) {
                alert('Ingrese el email');
                //$cordovaToast.show("Ingrese el email", 'long', 'bottom');
                return false;
            }
            if (!$scope.user.password) {
                alert('Ingrese la contraseña');
                //$cordovaToast.show("Ingrese la contraseña", 'long', 'bottom');
                return false;
            }

            $ionicLoading.show();
            $webServices.addStudent({
                'email': $scope.user.email,
                'password': $scope.user.password,
                'firstName': $scope.user.firstName,
                'lastName': $scope.user.lastName
            }).then(function(res){

                $ionicLoading.hide();
                $state.go('tabs.studentList');

            }, function(err){
                $ionicLoading.hide();
                alert(err.text);
                //$cordovaToast.show(err.text, 'long', 'bottom');
            })
        }

        $scope.goBack = function(){
            $state.go('tabs.studentList');
        }

    });

});
