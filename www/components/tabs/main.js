angular.module('app.tabs', [['components/tabs/controllers/tabsCtrl.js','components/tabs/css/style.css']])

.config(function($stateProvider) {

  $stateProvider
    .state('tabs', {
      url: '/tabs',
      abstract: true,
      cache: true,
      controller: 'TabsCtrl',
      templateUrl: 'components/tabs/templates/tabs.html'
    })

});


