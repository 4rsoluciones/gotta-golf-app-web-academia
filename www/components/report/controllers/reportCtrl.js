angular.module('app.report')

.controller('ReportCtrl', function ($scope, $rootScope, $ionicPlatform, $state, $stateParams, $ionicScrollDelegate, $webServices, $ionicLoading, $cordovaToast, $ionicModal){

  $ionicPlatform.ready(function () {

    $scope.report = $stateParams.report;

    $scope.expand = function(section){
      $scope.report[section] = $scope.report[section] ? false : true;
      $ionicScrollDelegate.resize();
    };

    $scope.viewInfo = function(media){
      $state.go('materialInfo', {media: JSON.stringify(media), fromReport: true});
    };

    var feedbackModal = null;

    $scope.openFeedback = function(value){
      $scope.feedback.value = value;
      feedbackModal.show();
    };

    $scope.sendFeedback = function() {

      if(!$scope.feedback.value){
        $cordovaToast.show("Ingrese su calificación", 'long', 'bottom');
        return
      }

      $ionicLoading.show();
      $webServices.addReportFeedback({
        feedback: {
          lesson: $scope.report.id,
          value: $scope.feedback.value,
          notes: $scope.feedback.notes
        }
      }).then(function(res){
        $scope.report.report.feedback = $scope.feedback;
        $scope.report.feedbackExpanded = true;
        $ionicLoading.hide();
        feedbackModal.hide();
      }, function(err){
        $ionicLoading.hide();
        $cordovaToast.show(err.text, 'long', 'bottom');
      })
    }

    $scope.closeFeedbackModal = function(){
      feedbackModal.hide();
    };

    $scope.feedback = {};
    $ionicModal.fromTemplateUrl('components/common/templates/feedbackModal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal){
      feedbackModal = modal;
    });

  });

});
