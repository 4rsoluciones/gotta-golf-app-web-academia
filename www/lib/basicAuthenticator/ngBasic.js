angular.module('ngBasicAuthenticator', ['base64', 'ngAccessControl'])

.factory('httpBasicInterceptor', function($base64, $q, $accessControl){

  return {
    'request': function(config){

      //verifica si se debe aplicar authorization basic
      if($accessControl.getAuthType(config.url) == 'basic'){

        //primera vez que crea el token (base64), posiblemente sea porque esta en pantalla login
        if(window.localStorage['username'] != undefined && window.localStorage['password'] != undefined){
          window.localStorage['AuthorizationBasic'] = $base64.encode(window.localStorage['username'] + ':' + window.localStorage['password'])

          //se elimina la credencial plana de localStorage
          window.localStorage.removeItem('username');
          window.localStorage.removeItem('password');
        }

        //token ya creado y guardado de forma local
        if(window.localStorage['AuthorizationBasic'] != undefined){
          angular.extend(config.headers, {'Authorization': 'Basic ' + window.localStorage['AuthorizationBasic']});
          angular.extend(config.headers, {'Content-Type': 'application/json'});
          try {
            angular.extend(config.headers, {'ACADEMY': JSON.parse(window.localStorage['academy']).id});
          }catch(e){}
        }

      }

      return config;
    },

    'requestError': function(rejection){
      return $q.reject(rejection);
    },

    'response': function(response){
      return response;
    },

    'responseError': function(rejection){
      return $q.reject(rejection);
    }
  }
})
