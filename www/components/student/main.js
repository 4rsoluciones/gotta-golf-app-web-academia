angular.module('app.student', [[
  'components/student/controllers/studentListCtrl.js',
  'components/student/controllers/newStudentCtrl.js',
  'components/student/css/style.css'
]])

  .config(function ($stateProvider) {

    $stateProvider
      .state('tabs.studentList', {
        url: '/studentList',
        cache: false,
        templateUrl: 'components/student/templates/studentList.html',
        controller: 'studentListCtrl',
        params: {
          noreload: false,
          keyword: null
        }
      })
      .state('newStudent', {
        url: '/newStudent',
        cache: false,
        templateUrl: 'components/student/templates/newStudent.html',
        controller: 'NewStudentCtrl'
      })

  });
