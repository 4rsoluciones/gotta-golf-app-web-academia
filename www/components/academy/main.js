angular.module('app.academy', [['components/academy/controllers/academyListCtrl.js', 'components/academy/css/style.css']])

  .config(function ($stateProvider) {

    $stateProvider
      .state('academyList', {
        url: '/academyList',
        cache: false,
        templateUrl: 'components/academy/templates/academyList.html',
        controller: 'AcademyListCtrl'
      })

  });
