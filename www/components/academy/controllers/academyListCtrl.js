angular.module('app.academy')

.controller('AcademyListCtrl', function($scope, $rootScope, $ionicPlatform, $timeout, $state, $ionicPopover, $dataHandler, $ionicLoading){

    $ionicPlatform.ready(function(){

        var popOptions = null;

        $timeout(function(){
            navigator.splashscreen.hide();
        }, 1000);

        $scope.flagPlatform = ionic.Platform.isAndroid() || ionic.Platform.device().platform == "Android";

        $scope.academyList = JSON.parse(window.localStorage['academyList'] || '[]');

        $scope.$on("$ionicView.beforeEnter", function(event, data){
            refresh();
        });

        $scope.select = function(academy){

            window.localStorage['academy'] = JSON.stringify(academy);

            if($rootScope.rol == 'teacher'){
                $ionicLoading.show();
                $dataHandler.getStudents().then(function(){
                    $ionicLoading.hide();
                    redirect();
                }, function(err){
                    $ionicLoading.hide();
                    if(err.status != 402){
                        redirect();
                    }
                })
            }else {
                $ionicLoading.show();
                $dataHandler.checkAcademyPayment().then(function(){
                    $ionicLoading.hide();
                    redirect();
                }, function(err){
                    $ionicLoading.hide();
                    if(err.status != 402){
                        redirect();
                    }
                })
            }
        };

        function redirect(){
            $rootScope.$broadcast('ReportListReload');
            $rootScope.$broadcast('reloadMaterial');
            $rootScope.$broadcast('ReloadAcademy');
            $state.go('tabs.reportList');
        }


        /************************** MENU DESPLEGABLE ***********************************/
        popOptions = $ionicPopover.fromTemplate('<div class="menu-custom"><div class="item" ng-click="optionSelected(\'refresh\')">Actualizar</div><div class="item" ng-click="optionSelected(\'contact\')">Contacto</div><div class="item" ng-click="optionSelected(\'logout\')">Cerrar Sesión</div></div>', {
            scope: $scope
        });

        $scope.openOptions = function($event) {
            popOptions.show($event);
        };

        $scope.optionSelected = function(option) {
            popOptions.hide();
            switch(option){
                case 'refresh':
                refresh();
                break;
                case 'contact':
                $state.go('contact');
                break;
                case 'logout':
                logout();
                $state.go('login');
                break;
            }

        };

        function refresh() {
            $ionicLoading.show();
            $dataHandler.getAcademies().then(function(){
                $scope.academyList = JSON.parse(window.localStorage['academyList'] || '[]');
                $ionicLoading.hide();
            }, function(){
                $ionicLoading.hide();
            })
        }

        function logout(){
            //$pushFactory.logout();
            window.localStorage.clear();
            $rootScope.userData = null;
            $rootScope.rol = null;
        }

    });

});
