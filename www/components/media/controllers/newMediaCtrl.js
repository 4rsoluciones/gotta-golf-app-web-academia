angular.module('app.media')

.controller('NewMediaCtrl', function($scope, $rootScope, $ionicPlatform, $ionicModal, $stateParams, $state, $ionicLoading, $webServices, $q, $driveApi, $youtubeApi, $goalHelper){

    $ionicPlatform.ready(function () {

        var actualProgress = 0;

        //var folderId = '0B82Bl53kiObQSlZMM2RoQ0lQZ3c';
        var folderId = '0Bz1MYMFIwE-4QVVlN1NzS1p3Qnc';

        $scope.media = {
            type: $stateParams.type,
            path: $stateParams.path
        };

        $scope.$on("$ionicView.beforeEnter", function(event, data) {
            //limpia filtro al cambiar de pantalla
            $rootScope.$broadcast('goalSearching', {goal: null, keyword: null});

            if($stateParams.media) {
                // console.log($stateParams.media);
                $scope.edit = true;
                $scope.media = $stateParams.media;
                console.log($scope.media.goals);
                $scope.goalSelected = $scope.media.goals;
            }

            //inicializa modal de objetivos al entrar
            $goalHelper.init($scope, true);
        });

        $scope.openGoalModal = function(){
            $goalHelper.show();
        };

        $scope.uploadFile = function(element) {
        	$scope.Iserror=false;
        	if(element.files[0].type.match(/image\/*/)){// check more condition with MIME type
                var reader = new FileReader();
                reader.onload = function(loadEvent) {
                    $scope.$apply(function() {
                        $scope.filepreview = loadEvent.target.result;
                        $scope.media.path = loadEvent.target.result;
                        $scope.media.name = element.files[0].name;
                        $scope.media.type = 'image';
                        $scope.media.mimetype = element.files[0].type;
                    });
                }
                reader.readAsDataURL(element.files[0]);
        	} else {
                $scope.filepreview = null;
                alert('El formato del archivo es incorrecto.');
            }
        };
        $scope.uploadVideo = function(element) {
        	$scope.Iserror=false;
        	if(element.files[0].type.match(/video\/*/)){// check more condition with MIME type
                var reader = new FileReader();
                reader.onload = function(loadEvent) {
                    $scope.$apply(function() {
                        console.log(loadEvent);
                        $scope.videoPreview = loadEvent.target.result;
                        var myVideo = document.getElementById('videoPreview');
                        myVideo.src = loadEvent.target.result;
                        myVideo.load();
                        $scope.media.path = loadEvent.target.result;
                        $scope.media.name = element.files[0].name;
                        $scope.media.type = 'video';
                        $scope.media.mimetype = element.files[0].type;
                    });
                }
                reader.readAsDataURL(element.files[0]);
        	} else {
                $scope.videoPreview = null;
                alert('El formato del archivo es incorrecto.');
            }
        };
        $scope.saveMedia = function(){

            if(!$scope.media.name){
                alert('Ingrese un nombre');
                //$cordovaToast.show("Ingrese un nombre", 'long', 'bottom');
                return
            }
            if(!$scope.media.description){
                alert('Ingrese una descripción');
                //$cordovaToast.show("Ingrese una descripción", 'long', 'bottom');
                return
            }
            if(!$scope.goalSelected.length){
                alert('Seleccione al menos un objetivo');
                //$cordovaToast.show("Seleccione al menos un objetivo", 'long', 'bottom');
                return
            }

            $ionicLoading.show({template: $scope.edit ? 'Guardando cambios' : 'Cargando: 0%'});

            uploadFile().then(function(res){

                if($scope.edit){

                    $webServices.editIndication($scope.media.id, {
                        indication: {
                            mediaMaterial: $scope.media.media_material,
                            name: $scope.media.name,
                            description: $scope.media.description,
                            requirements: $scope.media.requirements,
                            security: $scope.media.security,
                            alternatives: $scope.media.alternatives,
                            goals: getGoalsSelected()
                        }
                    }).then(function (res) {
                        $ionicLoading.hide();
                        $cordovaToast.show("Material modificado correctamente", 'long', 'bottom');
                        $rootScope.$broadcast('reloadMaterial');
                        try {
                            $state.go('tabs.materialHeader.' + {
                                'type.video': 'video',
                                'type.image': 'image',
                                'type.audio': 'sound'
                            }[$scope.media.media_material.type]);
                        }catch(e){
                            $state.go('tabs.materialHeader.video');
                        }
                    }, function (err) {
                        $ionicLoading.hide();
                        alert(err.text);
                        //$cordovaToast.show(err.text, 'long', 'bottom');
                    });



                }else {
                    $ionicLoading.show({template: 'Finalizando'});

                    $scope.media.uri = res.id;

                    $webServices.addIndication({
                        indication: {
                            mediaMaterial: {
                                type: {'video': 'type.video', 'image': 'type.image', 'sound': 'type.audio'}[$scope.media.type],
                                uri: $scope.media.uri
                            },
                            name: $scope.media.name,
                            description: $scope.media.description,
                            requirements: $scope.media.requirements,
                            security: $scope.media.security,
                            alternatives: $scope.media.alternatives,
                            goals: getGoalsSelected()
                        }
                    }).then(function (res) {
                        $ionicLoading.hide();
                        $cordovaToast.show("Material ingresado correctamente", 'long', 'bottom');
                        if ($stateParams.fromHome) {
                            $rootScope.$broadcast('reloadMaterial');
                            $state.go('tabs.materialHeader.' + $scope.media.type);
                        } else {
                            $rootScope.$broadcast('newMedia', {'media': res});
                            $state.go('newReport');
                        }
                    }, function (err) {
                        $ionicLoading.hide();
                        alert(err.text);
                        //$cordovaToast.show(err.text, 'long', 'bottom');
                    });
                }
            }, function(err){
                $ionicLoading.hide();
                alert("Ha ocurrido un error al subir el archivo");
                //$cordovaToast.show("Ha ocurrido un error al subir el archivo", 'long', 'bottom');
            }, function(progress){
                progress.new = Math.trunc(100 * progress.loaded / progress.total);
                actualProgress = actualProgress < progress.new ? progress.new : actualProgress;
                $ionicLoading.show({template: 'Cargando: '+ actualProgress +'%'});
            })

        };

        function uploadFile(){
            var d = $q.defer();
            var resolveUpload = true;

            if($scope.media.id) {
                d.resolve({});
                resolveUpload = false;
            }

            if($scope.media.uri) {
                d.resolve({id: $scope.media.uri});
                resolveUpload = false;
            }

            if(resolveUpload)
                $webServices.getAccessToken().then(function(res){

                    if(!res.accessTokens || res.accessTokens.length === 0) {
                        d.reject('token');
                        return d.promise;
                    }


                    for(var t=0; t<res.accessTokens.length; t++){
                        if(res.accessTokens[t].type == 'youtube')
                            window.localStorage['YOUTUBE_DATA'] = JSON.stringify({ access_token: res.accessTokens[t].access_token });
                        if(res.accessTokens[t].type == 'drive')
                            window.localStorage['DRIVE_DATA'] = JSON.stringify({ access_token: res.accessTokens[t].access_token });
                    }

                    switch ($scope.media.type) {

                        case 'sound':
                        case 'image':
                            $driveApi.uploadFile(
                                {
                                    location: $scope.media.path,
                                    title: $scope.media.name,
                                    media: $scope.media,
                                    folderId: [folderId]
                                },
                                $scope.media.type == 'sound' ? 'getFromRecorder' : null
                            ).then(
                                function (res) {
                                    if (res && res.id) {
                                        d.resolve({id: res.id});
                                    }else{
                                        d.reject();
                                    }
                                },
                                function (err) {
                                    d.reject(err);
                                },
                                function (progress) {
                                    d.notify(progress);
                                }
                            );
                            break;

                        case 'video':
                            $youtubeApi.uploadVideo({
                                location: $scope.media.path,
                                title: $scope.media.name,
                                mimetype: $scope.media.mimetype,
                                description: $scope.media.description,
                                privacy: 'Unlisted',
                                embeddable: true
                            }).then(
                                function (res) {
                                    if (res && res.id) {
                                        d.resolve({id: res.id});
                                    }else{
                                        d.reject();
                                    }
                                },
                                function () {
                                    d.reject();
                                },
                                function (progress) {
                                    d.notify(progress);
                                });
                                break;
                            }

                    }, function(err){
                        d.reject(err);
                    });

                return d.promise;
        }

        function getGoalsSelected(){
            var goalSelected = [];
            for(var i=0; i<$scope.goalSelected.length; i++)
                goalSelected.push($scope.goalSelected[i].id)
            return goalSelected;
        }
    })
});
