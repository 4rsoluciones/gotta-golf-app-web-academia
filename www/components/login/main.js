angular.module('app.login', [['components/login/controllers/loginCtrl.js', 'components/login/css/style.css']])

  .config(function ($stateProvider) {

    $stateProvider
      .state('login', {
        url: '/login',
        cache: false,
        templateUrl: 'components/login/templates/login.html',
        controller: 'LoginCtrl'
      })

  });
