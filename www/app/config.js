angular.module('app', ['ionic', 'oc.lazyLoad', 'youtube-embed', 'ngLocale', 'ngBasicAuthenticator'])

		.constant('$apiConfig', {
			'api': [
				//{'url': 'https://www.andresgotta.com.ar'}
				// TODO: cambiar url
				//{'url': 'https://php56.projectsunderdev.com/gotta-golf-site/web'},
				{'url': 'https://php56.projectsunderdev.com/gpstats-staging/web'},
				// {'url': 'http://192.168.2.104:81/gotta-golf-site/web'}
			],
			'accessControl': [
				{'url': '/api/recover/process', 'auth': 'public'},
				{'url': '/api/recover/request', 'auth': 'public'}
			],
			'defaultAuth': 'basic'
		})

		.value('$pushConfig', {
			'android': {
				'senderID': "899795119817"
			},
			'ios': {
				'alert': "true",
				'badge': "false",
				'sound': "true"
			}
		})

		.config(function ($ionicConfigProvider, $ocLazyLoadProvider, $sceDelegateProvider, $httpProvider) {

			//define la transicion entre las pantallas
			$ionicConfigProvider.views.transition("android");

			//deshabilita el swipe back en iOS
			$ionicConfigProvider.views.swipeBackEnabled(false);

			//establece 0 vistas en cache como maximo
			$ionicConfigProvider.views.maxCache(5);

			$ionicConfigProvider.scrolling.jsScrolling(true);

			$httpProvider.interceptors.push('httpBasicInterceptor');
			//TODO: ver de volver a activar cdo se modifique para aceptar varias app
			// $httpProvider.interceptors.push('httpVersionInterceptor');

			$sceDelegateProvider.resourceUrlWhitelist([
				// Allow same origin resource loads.
				'self',
				// Allow loading from our assets domain.  Notice the difference between * and **.
				'http://docs.google.com/**',
				'https://www.youtube.com/**'
			]);

			$ocLazyLoadProvider.config({
				debug: false
			});

		})
