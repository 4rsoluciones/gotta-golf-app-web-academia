angular.module('app.report')

.factory('$mediaHelper', function($ionicPlatform, $q, $cordovaFile, $timeout){


    return {
      getUploadedSize: getUploadedSize,
      getTotalMediaSize: getTotalMediaSize
    }

    function getUploadedSize(multimedia){

      var sum = 0;

      for(var i=0; i < multimedia.videoList.length; i++) {
        sum += multimedia.videoList[i].uploaded;
      }
      for(var i=0; i < multimedia.imageList.length; i++) {
        sum += multimedia.imageList[i].uploaded;
      }
      for(var i=0; i < multimedia.audioList.length; i++) {
        sum += multimedia.audioList[i].uploaded;
      }

      return sum;
    }

    function getTotalMediaSize(multimedia){
      var totalSize = 0;
      var sizePromises = [];
      var d = $q.defer();

      for(var i=0; i < multimedia.videoList.length; i++) {
        if(!multimedia.videoList[i].id)
          sizePromises.push(
            $cordovaFile.checkFile('', multimedia.videoList[i].path).then(function (res) {
              res.file(function (file) {
                totalSize += file.size;
              })
            }, function (err) {
              console.log(err);
            })
          )
      }

      for(var i=0; i < multimedia.imageList.length; i++) {

        if(!multimedia.imageList[i].id)
          sizePromises.push(
            $cordovaFile.checkFile('', multimedia.imageList[i].path).then(function (res) {
              res.file(function (file) {
                totalSize += file.size;
              })
            }, function (err) {
              console.log(err);
            })
          )
      }

      for(var i=0; i < multimedia.audioList.length; i++) {

        if(!multimedia.audioList[i].id)
          sizePromises.push(
            $cordovaFile.checkFile(cordova.file.dataDirectory, multimedia.audioList[i].path.split('/')[multimedia.audioList[i].path.split('/').length - 1]).then(function (res) {
              res.file(function (file) {
                totalSize += file.size;
              })
            }, function (err) {
              console.log(err);
            })
          )
      }

      $q.all(sizePromises).then(null, null).finally(function(){
        $timeout(function(){
          // console.log("-------------------- Total size: " + totalSize);
          d.resolve(totalSize);
        }, 1000)
      })

      return d.promise;
    }

});
