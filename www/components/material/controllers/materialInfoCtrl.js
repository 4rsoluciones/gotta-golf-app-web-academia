angular.module('app.material')

  .controller('MaterialInfoCtrl', function($scope, $rootScope, $ionicPlatform, $state, $stateParams, $timeout, $cordovaInAppBrowser, $cordovaToast, $ionicModal, $ionicPopup, $webServices, $ionicLoading, $ionicHistory){

    $ionicPlatform.ready(function () {

      $scope.media = JSON.parse($stateParams.media || '{}');
      $scope.fromReport = $stateParams.fromReport;
      $scope.fullScreenModal = null;
      $scope.isAndroid = ionic.Platform.isAndroid() || ionic.Platform.device().platform == 'Android';

      $scope.rotateScreen = function(){
        console.log(screen.orientation.type);
        switch(screen.orientation.type) {
          case 'portrait':
          case 'portrait-primary':
          case 'portrait-secondary':
            screen.orientation.lock('landscape');
            break;
          case 'landscape':
          case 'landscape-primary':
          case 'landscape-secondary':
            screen.orientation.lock('portrait');
            break;
        }
      };

      $ionicModal.fromTemplateUrl('components/common/templates/youtubeModal.html', {
        scope: $scope,
        animation: 'none'
      }).then(function(modal){
        $scope.fullScreenModal = modal;
      });

      $scope.$on('$destroy', function() {
        $scope.fullScreenModal.remove();
      });

      $scope.$on('modal.hidden', function() {
        screen.orientation.lock('portrait');
        $scope.showModal = false;
        $timeout(function() {
          document.getElementById("frameYoutube").src = 'https://www.youtube.com/embed/' + $scope.media.media_material.uri + '?modestbranding=1&autohide=1&showinfo=0&controls=1&rel=0&fs=1&cc_load_policy=0';
        });
      });

      $scope.closeModalYoutube = function(){
        $scope.fullScreenModal.hide();
      };

      $scope.openFullScreen = function(){
        $timeout(function(){
          document.getElementById("frameYoutube").src = '';
        });

        $scope.showModal = true;
        $scope.fullScreenModal.show();
      };

      $scope.deleteMedia = function(){

        $ionicPopup.show({
          title: 'Confirmar',
          subTitle: !$scope.media.has_reports ? '¿Desea eliminar la instrucción?' : 'Esta instrucción se utiliza en al menos un informe, ¿desea eliminarla de todas maneras?',
          cssClass: 'confirm-pop',
          scope: $scope,
          buttons: [
            {text: 'Cancelar'},
            {
              text: 'Aceptar',
              onTap: function (e) {
                $ionicLoading.show();
                $webServices.deleteMedia({'id': $scope.media.id}).then(function () {
                  $ionicLoading.hide();
                  $cordovaToast.show("Instrucción eliminada exitosamente", 'long', 'bottom');
                  $rootScope.$broadcast('reloadMaterial');
                  $ionicHistory.goBack();
                }, function (err) {
                  console.log(err);
                  $ionicLoading.hide();
                  $cordovaToast.show(err.text, 'long', 'bottom');
                })
              }
            }
          ]
        });
      }

      $scope.editMedia = function(){
        $state.go('newMedia',{media: $scope.media, fromHome: $rootScope.fromHome});
      }

    })

  });
