angular.module('app.common')

  .factory('$goalHelper', function($rootScope, $ionicPlatform, $ionicModal, $timeout, $webServices, $q, $ionicScrollDelegate, $ionicLoading){

    /************************** MODAL PARA OBJETIVOS ***********************************/

    var limit = 10;
    var offset = 0;
    var goalModal = null;
    var searchPromise = null;

    function getGoals(scope) {
      var d = $q.defer();

      scope.loadingGoals = true;

      $webServices.getGoals({
        limit: limit,
        offset: offset,
        query: scope.search.keyword
      }).then(function (res) {
        if (res) {
          if (res.goals.length < limit)
            scope.moreGoals = false;
          scope.goalList = [].concat(scope.goalList, res.goals);
          offset += limit;
        }

        d.resolve();
        scope.loadingGoals = false;
      }, function (err) {
        if(err && err.error == 'timeout')
          scope.moreGoals = false;

        d.reject();
        scope.loadingGoals = false;
      });

      return d.promise;
    }

    function init(scope, newGoal) {

      limit = 10;
      offset = 0;
      scope.loadingGoals = false;

      scope.goalList = [];
      if(!scope.goalSelected)
        scope.goalSelected = [];
      scope.moreGoals = true;
      scope.search = {keyword: null};

      scope.$on('cancelSearchTapped', function(){
        scope.goalSelected = [];
        scope.search.keyword = null;
        reloadGoals();
      });

      scope.hideGoalModal = function(){
        if(!newGoal) {
          $rootScope.$broadcast('goalSearching', {goal: null, keyword: scope.search.keyword});
          scope.goalSelected = [];
        }
        goalModal.hide();
      };

      scope.isSelected = function(id){
        for(var i=0; i<scope.goalSelected.length; i++){
          if(id == scope.goalSelected[i].id)
            return true;
        }

        return false;
      };

      function unselectGoal(id){
        for(var i=0; i<scope.goalSelected.length; i++){
          if(id == scope.goalSelected[i].id)
            scope.goalSelected.splice(i,1);
        }
      }

      scope.setGoal = function(goal){
        if(newGoal){
          //multiple seleccion
          if (scope.goalSelected.length && (scope.isSelected(goal.id))) {
            //presiona sobre el goal seleccionado
            unselectGoal(goal.id);
          } else {
            scope.goalSelected.push(goal);
          }

        } else {
          //unica seleccion
          if (scope.goalSelected.length && (goal.id == scope.goalSelected[0].id)) {
            //presiona sobre el goal seleccionado
            scope.goalSelected = [];
            goalModal.hide();
            $rootScope.$broadcast('cancelSearchTapped');
          } else {
            scope.search.keyword = null;
            scope.goalSelected = [goal];
            goalModal.hide();
            $rootScope.$broadcast('goalSearching', {goal: goal, keyword: null});
          }
        }
      };

      scope.loadMoreGoals = function() {
        if(!scope.loadingGoals) {
          getGoals(scope).then(null, null).finally(function () {
            $timeout(function () {
              scope.$broadcast('scroll.infiniteScrollComplete');
            }, 0)
          });
        }
      };

      scope.filterGoals = function(){
        $timeout.cancel(searchPromise);
        searchPromise = $timeout(function () {
          reloadGoals();
        }, 1500)
      };

      scope.newGoal = function(){
        if(scope.search.keyword) {
          $ionicLoading.show();
          $webServices.addGoal({goal: {name: scope.search.keyword}}).then(function (res) {
            if (!scope.isSelected(res.id))
              scope.goalSelected.push({id: res.id, name: scope.search.keyword});
            reloadGoals();
            scope.search.keyword = null;
            $ionicLoading.hide();
          }, function (err) {
            $ionicLoading.hide();
            $cordovaToast.show(err.text, 'long', 'bottom');
          })
        }
      };

      if(newGoal) {
        $ionicModal.fromTemplateUrl('components/common/templates/goalModal.html', {
          scope: scope,
          animation: 'slide-in-up'
        }).then(function (modal) {
          goalModal = modal;
        });
      }else{
        $ionicModal.fromTemplateUrl('components/common/templates/findGoalModal.html', {
          scope: scope,
          animation: 'slide-in-up'
        }).then(function (modal) {
          goalModal = modal;
        });
      }

      function reloadGoals(){
        offset = 0;
        scope.goalList = [];
        scope.moreGoals = true;
        $ionicScrollDelegate.resize();
        $ionicScrollDelegate.scrollBottom();
      }

    }

    return {

      init: function(scope, newGoal){
        init(scope, newGoal);
      },

      show: function(){
        goalModal.show();
      }
    }


  });
