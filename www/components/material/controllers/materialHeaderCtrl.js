angular.module('app.material')

  .controller('MaterialHeaderCtrl', function($scope, $rootScope, $ionicPlatform, $timeout, $state){

    $ionicPlatform.ready(function () {

      $scope.$on("$ionicView.beforeEnter", function(event, data){
        $rootScope.tabActive = 3;
      });

      $rootScope.subTabActive = 0;

      $scope.viewMaterialInfo = function(media){
        $state.go('materialInfo', {media: JSON.stringify(media)});
      };

      $scope.viewMaterialTab = function(state){
        $rootScope.$broadcast('reloadMaterial');
        $state.go(state);
      };

    })

  });
