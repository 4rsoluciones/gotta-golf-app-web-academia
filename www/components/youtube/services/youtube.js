angular.module('app.youtube')

.factory('$youtubeApi', function ($http, $q, $rootScope) {

    //url autorización
    var urlOauth2 = 'https://accounts.google.com/o/oauth2/auth';

    //permisos
    var scopeUrl = 'https://www.googleapis.com/auth/youtube.upload https://www.googleapis.com/auth/youtube https://www.googleapis.com/auth/youtubepartner';

    //url para solicitar un token
    var urlGetToken = 'https://accounts.google.com/o/oauth2/token';

    //datos de configuracion
    var configData = {};

    var getCodeFromUrl = function (url) {
        var regex = /[?&]([^=#]+)=([^&#]*)/g, match;
        while (match = regex.exec(url)) {
            if (match[1] == 'code') return match[2];
        }
        return null;
    };

    var getAccessCode = function () {
        /*var d = $q.defer(), code, browser;

        browser = $cordovaInAppBrowser.open(
            urlOauth2 + '?' +
            'client_id=' + configData.clientId +
            '&response_type=code' +
            '&scope=' + scopeUrl +
            '&redirect_uri=' + configData.redirectUri +
            '&access_type=offline',
            '_blank', {});

            //autorizó la api
            $rootScope.$on('$cordovaInAppBrowser:loadstart', function (e, event) {
                code = getCodeFromUrl(event.url);
                if (code) {
                    if (browser) $cordovaInAppBrowser.close();
                    d.resolve(code);
                }
            });

            //cierre intencional del navegador
            $rootScope.$on('$cordovaInAppBrowser:exit',
            function () {
                if (!code) d.reject();
                else d.resolve(code);
            });

            return d.promise;*/
        };

        //obtener token de acceso -> dura una hora y luego hay que renovarlo
        var getAccessToken = function (code){
            var d = $q.defer();
            $http({
                method: 'POST',
                url: urlGetToken,
                data: 'code=' + code + '&' + 'client_id=' + configData.clientId + '&' + 'client_secret=' + configData.clientSecret + '&' + 'redirect_uri='+ configData.redirectUri + '&grant_type=authorization_code',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(
                function (res) {
                    res.data.token_request = new Date().getTime(); //guardo la fecha en milisegundos
                    window.localStorage['YOUTUBE_DATA'] = JSON.stringify(res.data);
                    d.resolve();
                },
                function (err) {
                    console.log(err);
                    d.reject();
                }
            );

            return d.promise;
        };

        //recuperar access_token al expirar el mismo
        var refreshToken = function () {
            var youtubeTokenData = JSON.parse(window.localStorage['YOUTUBE_DATA']);
            var d = $q.defer();
            $http({
                method: 'POST',
                url: urlGetToken,
                data: 'client_id=' + configData.clientId + '&' + 'client_secret=' + configData.clientSecret + '&' + 'refresh_token=' + youtubeTokenData.refresh_token + '&' + 'grant_type=refresh_token',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(
                function (res) {
                    youtubeTokenData.access_token = res.data.access_token;
                    youtubeTokenData.expires_in = res.data.expires_in;
                    youtubeTokenData.token_type = res.data.token_type;
                    youtubeTokenData.token_request = new Date().getTime(); //guardo la fecha en milisegundos
                    window.localStorage['YOUTUBE_DATA'] = JSON.stringify(youtubeTokenData);
                    d.resolve();
                },
                function (err) {
                    console.log(err);
                    d.reject();
                }
            );

            return d.promise;
        };

        var verifyToken = function () {
            var youtubeTokenData = JSON.parse(window.localStorage['YOUTUBE_DATA']);
            var now = new Date().getTime();
            var diff_dates = now - youtubeTokenData.token_request;

            return (diff_dates < (youtubeTokenData.expires_in * 1000));
        };

        var getFileData = function (path) {
            var d = $q.defer();

            /*$cordovaFile.checkFile('', path).then(function (res) {
                res.file(function (file) {
                    d.resolve(file);
                });
            }, function (err) {
                console.log(err);

                //prueba enviando file://
                $cordovaFile.checkFile('', 'file://' + path).then(function (res) {
                    res.file(function (file) {
                        d.resolve(file);
                    });
                }, function (err) {
                    console.log(err);
                    d.reject();
                })

            });*/

            return d.promise;
        };

        function base64MimeType(encoded) {
            var result = null;

            if (typeof encoded !== 'string') {
                return result;
            }

            var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

            if (mime && mime.length) {
                result = mime[1];
            }

            return result;
        };

        return {

            setConfig: function(config){
                if(config){
                    configData = config;
                }
            },

            getCredentials: function () {
                var d = $q.defer();
                if (!window.localStorage.getItem('YOUTUBE_DATA')) { //primera vez que intenta acceder a la api
                    getAccessCode(configData.clientId, configData.redirectUri).then(
                        function (code) {
                            getAccessToken(code, configData.clientId, configData.clientSecret, configData.redirectUri).then(
                                function () {
                                    d.resolve();
                                },
                                function () {
                                    d.reject();
                                });
                            },
                            function () {
                                d.reject();
                            }
                        );
                    }
                    else {
                        if (verifyToken()) {
                            d.resolve();
                        }
                        else {
                            refreshToken(configData.clientId, configData.clientSecret).then(
                                function () {
                                    d.resolve();
                                },
                                function () {
                                    d.reject();
                                }
                            );
                        }
                    }

                    return d.promise;
                },

                //funcion para subir video
                // Tamaño máximo del archivo: 64 GB
                // Tipos MIME de los medios aceptados: video/*, application/octet-stream
                // Authorization: Bearer ACCESS_TOKEN
                uploadVideo: function (file) {

                    var d = $q.defer();

                    var youtubeTokenData = JSON.parse(window.localStorage['YOUTUBE_DATA']);
                    var url = 'https://www.googleapis.com/upload/youtube/v3/videos?part=snippet';

                    console.log(file);

                    data = file.location.split(',')[1];
                    mimeType = base64MimeType(file.location);

                    dataForm = "--myFile\nContent-Type: application/json; charset=UTF-8\n\n{\n\tsnippet: {\n\t\t\"title\": \""+file.title+"\",\n\t\t\"description\": \""+file.description+"\",\n\t\t\"contentType\": \""+file.mimetype+"\"\n\t}\n  \n}\n\n--myFile\nContent-Type: "+file.mimetype+"\nContent-Transfer-Encoding: base64\n\n"+data+"\n\n--myFile--\n";

                    var options = {
                        headers: {
                            'Authorization': 'Bearer ' + youtubeTokenData.access_token,
                            'Content-Type': 'multipart/related; boundary=myFile'
                        }
                    };
                    $http.post(url, dataForm, options).success(
                        function(res) {
                            var id = null;
                            try {
                                id = res.id;
                            } catch(e){}
                            d.resolve({id: id, item: file.item});
                        }
                    ).error(function(err){
                        console.log(err);
                        d.reject(err);
                    });

                    return d.promise;
                }
            };

        });
