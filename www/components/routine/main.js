angular.module('app.routine', [[
  'components/routine/controllers/routineListCtrl.js',
  'components/routine/controllers/routineCtrl.js',
  'components/routine/controllers/newRoutineCtrl.js',
  'components/routine/css/style.css'
]])

  .config(function ($stateProvider) {

    $stateProvider
      .state('tabs.routineList', {
        url: '/routineList',
        cache: true,
        templateUrl: 'components/routine/templates/routineList.html',
        controller: 'RoutineListCtrl'
      })
      .state('newRoutine', {
        url: '/newRoutine',
        cache: false,
        templateUrl: 'components/routine/templates/newRoutine.html',
        controller: 'NewRoutineCtrl',
        params:{
          routine: null,
          edit: null
        }
      })
      .state('routine', {
        url: '/routine',
        cache: false,
        templateUrl: 'components/routine/templates/routine.html',
        controller: 'RoutineCtrl',
        params: {
          routine: null
        }
      })

  });
