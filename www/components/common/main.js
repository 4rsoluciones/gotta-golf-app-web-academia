angular.module('app.common', [[
  'components/common/css/style.css',
  'components/common/services/mediaHelper.js',
  'components/common/services/goalHelper.js',
  'components/common/filters/filters.js'
]]);
