angular.module('app.media', [[
  'components/media/controllers/mediaCtrl.js',
  'components/media/controllers/headerMediaCtrl.js',
  'components/media/controllers/newMediaCtrl.js',
  'components/media/css/style.css'
]])

  .config(function ($stateProvider) {

    $stateProvider
      .state('headerMedia', {
        url: '/headerMedia',
        abstract: true,
        cache: false,
        controller: 'HeaderMediaCtrl',
        templateUrl: 'components/media/templates/header.html'
      })
      .state('headerMedia.video', {
        url: '/video',
        cache: false,
        templateUrl: 'components/media/templates/media.html',
        controller: 'MediaCtrl',
        params:{
          type: 'video'
        }
      })
      .state('headerMedia.image', {
        url: '/image',
        cache: false,
        templateUrl: 'components/media/templates/media.html',
        controller: 'MediaCtrl',
        params:{
          type: 'image'
        }
      })
      .state('headerMedia.sound', {
        url: '/sound',
        cache: false,
        templateUrl: 'components/media/templates/media.html',
        controller: 'MediaCtrl',
        params:{
          type: 'sound'
        }
      })
      .state('newMedia', {
        url: '/newMedia',
        cache: false,
        templateUrl: 'components/media/templates/newMedia.html',
        controller: 'NewMediaCtrl',
        params:{
          type: null,
          path: null,
          fromHome: null,
          media: null
        }
      })

  });
